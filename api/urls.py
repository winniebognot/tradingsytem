from django.urls import path
from .views import ItemList, ItemDetail, TransactionDetail, TransactionList

urlpatterns = [
    path('transaction/', TransactionList.as_view()),
    path('transaction/<int:pk>/', TransactionDetail.as_view()),
    path('item/', ItemList.as_view()),
    path('item/<int:pk>/', ItemDetail.as_view()),
]