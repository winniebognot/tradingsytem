from django.db import models


class Item(models.Model):
    name = models.CharField(unique=True, max_length=100)
    price = models.IntegerField(null=True)

    def __str__(self):
        return self.name


class Transaction(models.Model):
    description = models.CharField(unique=True, max_length=100)
    name = models.ForeignKey(Item, on_delete=models.CASCADE)
    qty = models.IntegerField(null=True)
    date_added = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name